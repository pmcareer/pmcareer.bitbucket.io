<div class="text-center">
    <h1>Recent Projects</h1>
    <a target="_blank" href="https://docs.google.com/presentation/d/1rvi4wlc5LUD11SZ965t-mH47c8UQ1sRb58lcixj1PCA/edit?usp=sharing">Click here for the complete list of projects</a>
    <div class="row justify-content-center" id="projects">        
        <!-- <div class="col-10">
            <div  class="row justify-content-center" >
                    
            </div>
        </div> -->
    </div>
</div>

<script>
    projects();
    function projects(){

        const target_div = document.getElementById("projects")
        const target_array = [
            ["MCO88","assets/img/projects/projects.png","2022","https://mco88.com","https://mco88.com/login"],
            ["HIMMAX","assets/img/projects/projects.png","2022", "https://dev.himmax.com"],
            ["DEPOSIT ASSURE","assets/img/projects/projects.png","2022","https://devthecrm.depositassure.com.au/"],
        ];
        for (let index = 0; index < target_array.length; index++) {
            let title = target_array[index][0];
            let image = target_array[index][1];
            let year = target_array[index][2];
            let first_link = target_array[index][3];
            let second_link = target_array[index][4];
            
            target_div.innerHTML += `
            <div class="col-md-4 col-12"  >  
                <div class="row justify-content-center" >
                    <div class="col-10" >
                        <section class="mx- my-1">
                            <div class="card">
                                <div class="card-body d-flex flex-row bg-dark text-white py-1">                        
                                    <img src="assets/img/logo.png" class="rounded-circle me-3" height="50px"
                                    width="50px" alt="avatar" />
                                    <div class="text-left">
                                        <h5 class="card-title font-weight-bold ">${title}</h5>
                                        <small class="">
                                            <i class="far fa-clock "></i>
                                        </small>
                                        ${year}
                                    </div>
                                </div>
                                <div class="bg-image hover-overlay ripple rounded-0 text-center zoom py-2">
                                    <img class="img-fluid" src="${image}" style="width: 70px; height: 70px; min-height: 70px"
                                    alt="${title}"/>
                                    <a href="#!">
                                    <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                                    </a>
                                </div>                                
                                <div class="text-success">
                                    <a href="${first_link}">More Info</a>
                                </div>
                            </div>
                        </section>
                    </div>                        
                </div> 
                
            </div>       
            `
        }
    }    
</script>


