<div class="text-center mb-5">
    <h1 class="">Tech Stack </h1> 
    <div class="row justify-content-center ">
        <!-- 1st -->
        <!-- <div class="col-md-3 col-6">
            <section class="mx-auto my-5" style="max-width: 23rem; max-height:23rem;">
                <div class="card">
                <div class="card-body d-flex flex-row">
                    <img src="assets/img/logo.png" class="rounded-circle me-3" height="50px"
                    width="50px" alt="avatar" />
                    <div>
                    <h5 class="card-title font-weight-bold mb-2">Javascript</h5>
                    <p class="card-text"><i class="far fa-clock pe-2"></i>2017</p>
                    </div>
                </div>
                <div class="bg-image hover-overlay ripple rounded-0 text-center zoom">
                    <img class="img-fluid" src="assets/img/skills/javascript.png" style="width: 150px; height: 150px;"
                    alt="javascript skill"/>
                    <a href="#!">
                    <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                    </a>
                </div>
                <div class="card-body">
                    
                </div>
                </div>
            </section>
        </div>    -->
        <div class="col-md-4 col-12" >      
            <img src="assets/img/skills/coding.png" class="text-danger my-3 zoom pulse " style="width: 100px;" alt="avatar" />
            <div class="row justify-content-center" id="frontend">
            </div>
        </div>
        <div class="col-md-4 col-12">            
            <img src="assets/img/skills/framework.png" class="text-danger my-3 zoom pulse " style="width: 100px;" alt="avatar" />
            <div class="row justify-content-center" id="backend">

            </div>
        </div>
        <div class="col-md-4 col-12">            
            <img src="assets/img/skills/backend.png" class="text-danger my-3 zoom pulse " style="width: 100px;" alt="avatar" />
            <div class="row justify-content-center" id="tools">

            </div>
        </div>


        


    </div>
</div>

<script>
    const skill_array = [
        ["frontend","Android","assets/img/skills/android.png","2017"],
        ["frontend","PHP","assets/img/skills/php.png","2017"],
        ["frontend","Javascript","assets/img/skills/javascript.png","2017"],
        ["frontend","HTML","assets/img/skills/html5.png","2017"],
        ["frontend","CSS","assets/img/skills/css3.png","2017"],
        ["frontend","C#","assets/img/skills/c#.png","2015"],
        ["frontend","JAVA","assets/img/skills/java.png","2014"],

        ["backend","Laravel","assets/img/skills/laravel.png","2018"],
        ["backend","React Js","assets/img/skills/bootstrap.png","2022"],
        ["backend","Vue Js","assets/img/skills/bootstrap.png","2022"],
        ["backend","Bootstrap ","assets/img/skills/bootstrap.png","2018"],

        ["tools","CRM","assets/img/skills/payment.png","2022"],
        ["tools","Payment Solutions","assets/img/skills/payment.png","2022"],
        ["tools","Kissflow","assets/img/skills/kissflow.png","2022"],
        ["tools","Outsystem","assets/img/skills/outsystem.png","2022"],
        ["tools","Git","assets/img/skills/git.png","2018"],
    ];
    
    for (let index = 0; index < skill_array.length; index++) {
        let type = skill_array[index][0];
        let title = skill_array[index][1];
        let image = skill_array[index][2];
        let year = skill_array[index][3];
        let target_div = document.getElementById("frontend")
        if(type=="frontend"){
            target_div = document.getElementById("frontend")
        } 
        if(type=="backend"){
            target_div = document.getElementById("backend")
        } 
        if(type=="tools"){
            target_div = document.getElementById("tools")
        } 
        
        
        target_div.innerHTML += `
        <div class="col-10">
            <section class="mx- my-1">
                <div class="card">
                    <div class="card-body bg-dark text-white py-1" >  
                        <div class="row">
                            <div class="col-3">
                                <img src="assets/img/logo.png" class="align-self-start rounded-circle me-3" height="50px"
                                width="50px" alt="avatar" />
                            </div>                  
                            <div class="col-9 text-start bg-">
                                <h5 class="card-title font-weight-bold my-0 ">${title}</h5>                                
                                <small class="">
                                    <i class="far fa-clock "></i>
                                </small>
                                ${year}     
                            </div>
                        </div>   
                    </div>
                    <div class="bg-image hover-overlay ripple rounded-0 text-center zoom py-2">
                        <img class="img-fluid" src="${image}" style="width: 70px; height: 70px; min-height: 70px"
                        alt="${title}"/>
                        <a href="#!">
                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                        </a>
                    </div>
                </div>
            </section>
        </div>       
        `
    }

    
</script>


