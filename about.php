<!-- About-->
<section class="content-section bg-transparent" id="about">
    <div class="container px-4 px-lg-5 text-center">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-lg-10">
                <!-- Header-->
                <header class="d-flex align-items-center">
                <div class="container px-4 px-lg-5 text-center">                
                    <div class="text-center ">
                        <img src="assets/img/logo.png" class="img-fluid rounded-circle mb-2 bg- social-link  zoom pulse" alt="Profile Picture" style="width: 100px; height: 100px;">
                        <img src="default/images/logo.jpg" alt="" width="150">
                        <h2 class="ml1">
                            <img src="">
                            <span class="text-wrapper">
                            <span class="line line1"></span>
                            <span class="letters text-default">
                                <h2> 
                                Hi, I'm <b class="text-danger"> Paolo! </b> 

                                </h2>
                            </span>
                            <span class="line line2"></span>
                            </span>
                        </h2> 
                        <p class=" text-center text-contrail " >
                            <strong><span 
                            style="color: #175216;"
                                class="type-me txt-rotate"
                                ></span></strong>
                        </p>
                    </div>   
                </div>
                </header>
                <h2>
                    I'm a full-stack web and mobile developer
                </h2>
                <p class="lead mb-5">
                    Let's build something wonderful!
                </p>
                <a class="btn btn-dark btn zoom" href="http://m.me/paolowoi">Contact Me</a>
    
                <br> or simply send an email @ <a href="mailto:paolomaico.career@gmail.com">paolomaico.career@gmail.com</a> 
            </div>
        </div>
    </div>
</section>