<a class="menu-toggle rounded" href="#"><i class="fas fa-bars"></i></a>
<nav id="sidebar-wrapper" class="bg-black">
    <ul class="sidebar-nav">
        <li class="sidebar-brand"><a href="#page-top">What is it?</a></li>
        <li class="sidebar-nav-item"><a href="#page-top">Home</a></li>
        <li class="sidebar-nav-item"><a href="#portfolio">Tech Stack</a></li>
        <li class="sidebar-nav-item"><a href="#projects">Projects</a></li>
    </ul>
</nav>