<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>Paolo Maico Career</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="shorcut icon" type="image/x-icon" href="/default/images/logo_icon.ico" />

     <!-- Font Awesome icons (free version)-->
     <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
     <!-- Simple line icons-->
     <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css" rel="stylesheet" />
     <!-- Google fonts-->
     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/text-animation.css" rel="stylesheet" />
    <link href="css/card.css" rel="stylesheet" />
    <link href="css/fonts.css" rel="stylesheet" />
    <link href="css/loader.css" rel="stylesheet" />
    <style type="text/css"> 
        .container, .row.justify-content-center.align-items-center {
          height: 100%;
          min-height: 100%;
        }           
        body {
            max-width: 100%;
            overflow-x: hidden;     
            height: 100%;
            margin: 0; 
          /* overflow: hidden;  DISABLE SCROLL TO REMOVE BUG ON TYPE TEXT ANIMATION */
          /* font: normal 75% Arial, Helvetica, sans-serif; */
        }
        #particles-js {
          position: fixed;
          z-index: -1;
          /* position: absolute; */
          width: 100%;
          height: 100%;
          /* background-color: #fafafa;  */
          /* background-color: #383838;  */
          background-image: url("");
          background-repeat: no-repeat;
          background-size: cover;
          background-position: 50% 50%;
        }
    </style>
</head>
<body id="page-top" class="">
  
<div id="particles-js"></div>
<div class="container-fluid">  
    <div class="row" id="loading_div">
      <div class="col-12 text-center">
        <div id="loading_div2" class="bg-">
          <img id="loading-image" style="height: 200px; margin-top: 50%;" src="assets/img/logo.png" class="pulse1 mt-5 bg-" alt="Loading..." />
           <br>
           <h5>Loading</h5>
        </div>
      </div>
    </div>
     
    <div class="row" id="content">
      <div class="col-12">
        <!-- Navigation-->
        <?php include 'navbar.php';?>
        
        <!-- About-->
        <?php include 'about.php';?>
        <!-- Tech Stacks-->
        <?php include 'skills.php';?>
        <div class="mb-5">
          <!-- Portfolio-->
          <?php include 'projects.php';?>
        </div>
        <!-- Map-->
        <!-- <div class="mb-5">
          <h1> Find Me Here! </h1>
          <div class="map" id="contact">
            
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.028311617238!2d120.97560901436016!3d14.654334379687459!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b548c461ecd7%3A0xdeb7806b253f4195!2sCyPao%20Foodtrips!5e0!3m2!1sen!2sph!4v1645241999870!5m2!1sen!2sph" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              <br />
              <small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a></small>
          </div>
        </div> -->
        <!-- Footer-->
        <?php include 'footer.php';?>
        
      </div>
    </div> 
    <!-- end of row -->
</div>
<!-- /.login-box -->


   <!-- Scroll to Top Button-->
   <a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
   <!-- Bootstrap core JS-->
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
   <!-- Core theme JS-->
   <script src="js/scripts.js"></script>


<!-- TITLE TEXT ANIMATION  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
<script>
    // Wrap every letter in a span
    var textWrapper = document.querySelector('.ml1 .letters');
    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<h2 class='letter'>$&</h2>");

    anime.timeline({loop: false})
      .add({
        targets: '.ml1 .letter',
        scale: [0.3,1],
        opacity: [0,1],
        translateZ: 0,
        easing: "easeOutExpo",
        duration: 600,
        delay: (el, i) => 70 * (i+1)
      }).add({
        targets: '.ml1 .line',
        scaleX: [0,1],
        opacity: [0.5,1],
        easing: "easeOutExpo",
        duration: 700,
        offset: '-=875',
        delay: (el, i, l) => 80 * (l - i)
      })
      ;
</script>
<!-- TYPING TEXT -->
<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.11"></script>
<script>
  var typed = new Typed('.type-me', {
    strings: [ 
              "PASSION",
              "PROGRAMMING",
              "WEB DEVELOPMENT",
              "ANDROID DEVELOPMENT",
              "AGILE"
              ],
    typeSpeed: 50,
    backSpeed: 100,
    loop: true,
    loopCount: Infinity,
  });
</script>

<!-- particle  -->
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script src="https://threejs.org/examples/js/libs/stats.min.js"></script>
<script>
  // $(function() {
    //This is required for THE BUIG
    // setTimeout(function(){ 
    particlesJS("particles-js", {
    "particles": {
      "number": {
        "value": 80,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#428bca" //428bca //ffffff
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#5bc0de" //000000 //5bc0de
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.5,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 3,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 150,
        "color": "#5bc0de",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 6,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
  });


      
    // }, 200);
            //end of settimeout
  // });
</script>

<script>
  function isLooading(isloading = false) {
    var x = document.getElementById("loading_div");
    var content = document.getElementById("content");
    if(isloading==true){
      x.style.display = "block";
      content.style.display = "none";
    }else{
      x.style.display = "none";
      content.style.display = "block";
    }
  }
  console.log("Loading")
  isLooading(true)
  window.addEventListener("load", function() {
    console.log("Loaded")
    setTimeout(function(){
      isLooading(false)
    },1000);
    
  }, false); 
</script>

</body>
</html>